from typing import List
from datetime import date
from operator import or_
from functools import reduce
from collections import defaultdict
import operator

from django.db import models
from django.db.models import Count
from django.conf import settings
from django.utils import timezone

from wagtail.core.models import Page, Orderable, Collection
from wagtail.images.models import Image
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from wagtail.core import blocks
from wagtail.core.blocks.struct_block import StructValue
from wagtail.core.fields import RichTextField, StreamField
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.documents.edit_handlers import DocumentChooserPanel
from wagtail.admin.edit_handlers import (
    FieldPanel, StreamFieldPanel, PageChooserPanel,
    FieldRowPanel, InlinePanel, MultiFieldPanel, TabbedInterface, ObjectList
)
from wagtail.images.blocks import ImageChooserBlock
from modelcluster.fields import ParentalKey
from wagtail.contrib.forms.models import AbstractEmailForm, AbstractFormField
from wagtail.snippets.models import register_snippet

from wagtail.search import index
from wagtailmarkdown.blocks import MarkdownBlock

from modelcluster.fields import ParentalKey
from modelcluster.contrib.taggit import ClusterTaggableManager
from taggit.models import TaggedItemBase, Tag

from .blocks import (
    H2Block, H3Block, TitleSectionBlock, SectionBlock, MealBlock,
    NavigationBlock, ContactInfoBlock, ContactIconBlock, MealListBlock,
    BlockQuoteBlock, IllustratedParagraphBlock, NavigationBlock, LinkListBlock,
    IngredientBlock
)
from .forms import FormBuilder


class HomePage(Page):
    intro = RichTextField(blank=True)
    body = StreamField([
        ('titre', TitleSectionBlock()),
        ('section', SectionBlock()),
        ('plat', MealBlock()),
        ('carte', blocks.PageChooserBlock(
            model_type='home.MealCategoryPage',
            template='blocks/meal_category.html')),
    ])

    # blog_page = models.ForeignKey('home.BlogIndexPage',
    #                               on_delete=models.SET_NULL, null=True,
    #                               blank=True, related_name='+')
    # event_page = models.ForeignKey('home.EventIndexPage',
    #                                on_delete=models.SET_NULL, null=True,
    #                                blank=True, related_name='+')
    # contact_form = models.ForeignKey('home.FormPage',
    #                                  on_delete=models.SET_NULL, null=True,
    #                                  blank=True, related_name='+')

    contact_details = StreamField([
        ('icon', ContactIconBlock()),
    ], blank=True)

    footer_links = StreamField([
        ('column', LinkListBlock()),
    ])

    logo = models.ForeignKey('wagtailimages.Image', on_delete=models.SET_NULL,
                             related_name='+', null=True)
    collection = models.ForeignKey(Collection, on_delete=models.SET_NULL,
                                   related_name='+', null=True, blank=True)

    contact_form = models.ForeignKey('home.FormPage',
                                     on_delete=models.SET_NULL, null=True,
                                     blank=True, related_name='+')
    # navigation = StreamField([('nav', NavigationBlock())])

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname='full'),
        StreamFieldPanel('body'),
        PageChooserPanel('contact_form'),
    ]

    # link_panels = [
    #     PageChooserPanel('blog_page'),
    #     PageChooserPanel('features_page'),
    #     PageChooserPanel('event_page'),
    #     PageChooserPanel('contact_form'),
    # ]

    promote_panels = Page.promote_panels + [
        ImageChooserPanel('logo'),
        FieldPanel('collection'),
        StreamFieldPanel('contact_details'),
        StreamFieldPanel('footer_links'),
        # StreamFieldPanel('navigation'),
    ]

    edit_handler = TabbedInterface([
        ObjectList(content_panels, heading='Contenu'),
        # ObjectList(link_panels, heading='Linked content'),
        ObjectList(promote_panels, heading='Promotion'),
        ObjectList(Page.settings_panels, heading='Paramètres',
                   classname="settings"),
    ])

    search_fields = Page.search_fields + [
        index.SearchField('intro'),
        index.SearchField('body'),
    ]

    def get_context(self, request):
        context = super().get_context(request)
        context['home_page'] = True
        context['banner_images'] = Image.objects.filter(
            collection=self.collection
        )
        return context

    def get_blog(self):
        return self.get_children().type(BlogIndexPage).live()


class BlogIndexPage(Page):
    intro = RichTextField(blank=True)

    collection = models.ForeignKey(Collection, on_delete=models.SET_NULL,
                                   related_name='+', null=True, blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname='full'),
    ]

    promote_panels = Page.promote_panels + [
        FieldPanel('collection'),
    ]

    parent_page_types = ['home.HomePage']
    max_count = 1
    subpage_types = ['home.BlogCategoryPage', 'home.BlogPage']

    search_fields = Page.search_fields + [
        index.SearchField('intro'),
    ]

    def get_first_blogpages(self):
        return self.get_blogpages()[:3]

    def get_blogpages(self):
        return self.get_descendants().live().type(BlogPage)\
            .order_by('-blogpage__date').order_by('-id')

    def get_categories(self):
        return self.get_children().live().type(BlogCategoryPage)

    def get_all_categories(self):
        return self.get_descendants().live().type(BlogCategoryPage)

    def get_context(self, request):
        context = super().get_context(request)
        children = self.get_blogpages()

        search = request.GET.get('search')
        if search:
            children = children.search(search)
            context['search'] = search

        tag = request.GET.get('tag')
        if tag:
            children = children.filter(blogpage__tags__name=tag)
            context['active_tag'] = tag

        paginator = Paginator(children, settings.BLOG_ITEMS_PER_PAGE)
        context['paginator'] = paginator
        page = request.GET.get('page')
        try:
            context['articles'] = paginator.page(page)
        except PageNotAnInteger:
            context['articles'] = paginator.page(1)
        except EmptyPage:
            context['articles'] = paginator.page(paginator.num_pages)

        context['blog_index'] = self.page_ptr
        context['popular'] = BlogPage.objects.order_by('-views')[:5]
        context['tags'] = Tag.objects.annotate(num_articles=Count('blogpage'))\
            .order_by('-num_articles')
        return context


class BlogCategoryPage(Page):
    intro = RichTextField(blank=True)

    collection = models.ForeignKey(Collection, on_delete=models.SET_NULL,
                                   related_name='+', null=True, blank=True)
    image = models.ForeignKey('wagtailimages.Image', on_delete=models.PROTECT,
                              related_name='+', null=True)

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname='full'),
        ImageChooserPanel('image'),
    ]

    promote_panels = Page.promote_panels + [
        FieldPanel('collection'),
    ]

    parent_page_types = ['home.BlogIndexPage', 'home.BlogCategoryPage']
    subpage_types = ['home.BlogCategoryPage', 'home.BlogPage']

    search_fields = Page.search_fields + [
        index.SearchField('intro'),
    ]

    def get_first_blogpages(self):
        return self.get_blogpages()[:3]

    def get_blogpages(self):
        return self.get_descendants().live().type(BlogPage)\
            .order_by('-blogpage__date').order_by('-id')

    def get_categories(self):
        return self.get_children().live().type(BlogCategoryPage)

    def get_all_categories(self):
        return self.get_descendants().live().type(BlogCategoryPage)

    def get_context(self, request):
        context = super().get_context(request)
        children = self.get_blogpages()

        search = request.GET.get('search')
        if search:
            children = children.search(search)
            context['search'] = search

        tag = request.GET.get('tag')
        if tag:
            children = children.filter(blogpage__tags__name=tag)
            context['active_tag'] = tag

        paginator = Paginator(children, settings.BLOG_ITEMS_PER_PAGE)
        context['paginator'] = paginator
        page = request.GET.get('page')
        try:
            context['articles'] = paginator.page(page)
        except PageNotAnInteger:
            context['articles'] = paginator.page(1)
        except EmptyPage:
            context['articles'] = paginator.page(paginator.num_pages)
        context['blog_index'] = self.get_ancestors().live()\
            .type(BlogIndexPage).first()
        context['popular'] = BlogPage.objects.order_by('-views')[:5]
        context['tags'] = Tag.objects.annotate(num_articles=Count('blogpage'))\
            .order_by('-num_articles')
        return context

    @property
    def blogpages_count(self):
        return len(self.get_children().live().type(BlogPage))


class BlogPageTag(TaggedItemBase):
    content_object = ParentalKey('home.BlogPage', on_delete=models.CASCADE,
                                 related_name='tagged_items')


class BlogPage(Page):
    date = models.DateField('Post date', default=date.today)
    intro = models.CharField(max_length=250)
    body = StreamField([
        ('h2', H2Block()),
        ('h3', H3Block()),
        ('paragraph', blocks.RichTextBlock()),
        ('markdown', MarkdownBlock(icon='code')),
        ('image', ImageChooserBlock()),
    ])
    image = models.ForeignKey('wagtailimages.Image', on_delete=models.PROTECT,
                              related_name='+', null=True)

    tags = ClusterTaggableManager(through=BlogPageTag, blank=True)
    views = models.PositiveIntegerField(default=0)

    content_panels = Page.content_panels + [
        FieldPanel('date'),
        FieldPanel('intro'),
        ImageChooserPanel('image'),
        StreamFieldPanel('body'),
    ]

    search_fields = Page.search_fields + [
        index.SearchField('intro'),
        index.SearchField('body'),
        index.FilterField('date'),
    ]

    promote_panels = Page.promote_panels + [
        FieldPanel('tags'),
    ]

    parent_page_types = ['home.BlogIndexPage', 'home.BlogCategoryPage']
    subpage_types: List[str] = []

    @property
    def feed_image(self):
        return self.image

    def get_context(self, request):
        context = super().get_context(request)
        context['blog_index'] = self.get_ancestors().live()\
            .type(BlogIndexPage).first()
        context['popular'] = BlogPage.objects.order_by('-views')[:5]
        context['tags'] = Tag.objects.annotate(num_articles=Count('blogpage'))\
            .order_by('-num_articles')
        return context

    def next_blogpage(self):
        try:
            return BlogPage.objects.filter(date__gte=self.date,
                                           id__gt=self.id)\
                .exclude(id=self.id)\
                .order_by('date', 'id').first()
        except BlogPage.DoesNotExist:
            return None

    def previous_blogpage(self):
        try:
            return BlogPage.objects.filter(date__lte=self.date,
                                           id__lt=self.id)\
                .exclude(id=self.id)\
                .order_by('-date', '-id').first()
        except BlogPage.DoesNotExist:
            return None

    def serve(self, request):
        self.views += 1
        self.save()
        return super().serve(request)


class MealCategoryPage(Page):

    collection = models.ForeignKey(Collection, on_delete=models.SET_NULL,
                                   related_name='+', null=True, blank=True)

    intro = RichTextField(blank=True)

    extra_text = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname='full'),
        FieldPanel('extra_text'),
    ]

    parent_page_types = ['home.HomePage', 'home.MealCategoryPage']
    subpage_types = ['home.MealPage',
                     'home.MealCategoryPage',
                     'home.MenuCategoryPage']

    search_fields = Page.search_fields + [
        index.SearchField('intro'),
    ]

    promote_panels = Page.promote_panels + [
        FieldPanel('collection'),
    ]

    def get_meals(self):
        meals = self.get_descendants().live().type(MealPage)
        return meals

    def get_direct_meals(self):
        meals = self.get_children().live().type(MealPage)
        return meals

    def get_subcategories(self):
        categories = self.get_children().live().not_type(MealPage)\
            .not_type(MenuPage)
        return categories

    @property
    def random_meal(self):
        meals = self.get_meals()
        random_meal = meals.order_by('?').first()
        return random_meal

    @property
    def random_direct_meal(self):
        meals = self.get_direct_meals()
        random_meal = meals.order_by('?').first()
        return random_meal

    def get_extra_text(self):
        extra_text = []
        parent = self.get_parent().specific
        page = self
        while parent:
            try:
                if page.extra_text:
                    extra_text.insert(0, page.extra_text)
            except AttributeError:
                pass
            page = parent
            parent = page.get_parent()
            if parent:
                parent = parent.specific
        return extra_text


class MealPage(Page):
    body = RichTextField(blank=True)
    image = models.ForeignKey('wagtailimages.Image', on_delete=models.SET_NULL,
                              related_name='+', null=True)
    price = models.FloatField()
    ingredients = StreamField([
        ('ingredient', IngredientBlock()),
    ], blank=True)
    diet = models.CharField(choices=(('omni', 'Omnivore'),
                                     ('veggie', 'Végétarien'),
                                     ('vegan', 'Végane')),
                            max_length=10, default='omni')
    gluten_free = models.BooleanField(default=False)

    content_panels = Page.content_panels + [
        FieldPanel('body'),
        ImageChooserPanel('image'),
        FieldPanel('price'),
        StreamFieldPanel('ingredients'),
        FieldPanel('diet'),
        FieldPanel('gluten_free')
    ]

    search_fields = Page.search_fields + [
        index.SearchField('body'),
    ]

    parent_page_types = ['home.MealCategoryPage']
    subpage_types: List[str] = []

    @property
    def allergenes(self):
        alg = []
        for ingredient in filter(lambda i: dict(i.value)['is_allergenic'],
                                 self.ingredients):
            ingredient = dict(ingredient.value)
            alg.append(ingredient['allergene'] or ingredient['ingredient'])
        return alg

    def get_extra_text(self):
        extra_text = []
        page = self.get_parent().specific
        parent = page.get_parent().specific
        while parent:
            try:
                if page.extra_text:
                    extra_text.insert(0, page.extra_text)
            except AttributeError:
                pass
            page = parent
            parent = page.get_parent()
            if parent:
                parent = parent.specific
        return extra_text

class MenuCategoryPage(Page):

    collection = models.ForeignKey(Collection, on_delete=models.SET_NULL,
                                   related_name='+', null=True, blank=True)

    intro = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname='full'),
    ]

    parent_page_types = ['home.MealCategoryPage']
    max_count = 1
    subpage_types = ['home.MenuPage']

    search_fields = Page.search_fields + [
        index.SearchField('intro'),
    ]

    promote_panels = Page.promote_panels + [
        FieldPanel('collection'),
    ]

    def get_categories(self):
        return self.get_descendants().live().type(MenuPage)

    def get_menus(self):
        menus = self.get_descendants().live().type(MenuPage)
        return menus

    def get_direct_menus(self):
        menus = self.get_children().live().type(MenuPage)
        return menus

    @property
    def random_menu(self):
        menus = self.get_menus()
        random_menu = menus.order_by('?').first()
        return random_menu


class MenuPage(Page):
    body = RichTextField(blank=True)
    image = models.ForeignKey('wagtailimages.Image', on_delete=models.SET_NULL,
                              related_name='+', null=True)
    price = models.FloatField()
    categories = StreamField([
        ('category', blocks.PageChooserBlock(target_model=MealCategoryPage)),
        ('meals', MealListBlock()),
    ], blank=True)

    collection = models.ForeignKey(Collection, on_delete=models.SET_NULL,
                                   related_name='+', null=True, blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('body'),
        ImageChooserPanel('image'),
        FieldPanel('price'),
        StreamFieldPanel('categories'),
    ]

    search_fields = Page.search_fields + [
        index.SearchField('body'),
    ]

    promote_panels = Page.promote_panels + [
        FieldPanel('collection'),
    ]

    parent_page_types = ['home.MenuCategoryPage']
    subpage_types: List[str] = []

    @property
    def diet(self):

        def get_meal_qs(item):
            if isinstance(item.value, Page):
                return [i.specific for i in
                        item.value.get_descendants().type(MealPage)]
            if isinstance(item.value, StructValue):
                return item.value['meals']
            return []

        meals = reduce(operator.add, map(get_meal_qs, self.categories), [])
        # meals = reduce(or_, meals)
        if all([m.diet == 'vegan' for m in meals]):
            return 'vegan'
        if all([m.diet in ('veggie', 'vegan') for m in meals]):
            return 'veggie'
        return 'omni'


class StandardPage(Page):
    body = StreamField([
        ('titre', TitleSectionBlock()),
        ('section', SectionBlock()),
        ])
    collection = models.ForeignKey(Collection, on_delete=models.SET_NULL,
                                   related_name='+', null=True, blank=True)

    content_panels = Page.content_panels + [
        StreamFieldPanel('body'),
    ]

    promote_panels = Page.promote_panels + [
        FieldPanel('collection'),
    ]

    search_fields = Page.search_fields + [
        index.SearchField('body'),
    ]


class FormField(AbstractFormField):
    page = ParentalKey('FormPage', on_delete=models.CASCADE,
                       related_name='form_fields')
    css_class = models.CharField(max_length=30, blank=True)

    panels = AbstractFormField.panels + [
        FieldPanel('css_class'),
    ]


class FormPage(AbstractEmailForm):

    collection = models.ForeignKey(Collection, on_delete=models.SET_NULL,
                                   related_name='+', null=True, blank=True)

    image = models.ForeignKey('wagtailimages.Image', on_delete=models.SET_NULL,
                              related_name='+', null=True)

    form_builder = FormBuilder

    intro = StreamField([
        ('h2', H2Block()),
        ('h3', H3Block()),
        ('paragraph', blocks.RichTextBlock()),
        ('markdown', MarkdownBlock(icon='code')),
        ('image', ImageChooserBlock()),
    ])
    thank_you_text = StreamField([
        ('h2', H2Block()),
        ('h3', H3Block()),
        ('paragraph', blocks.RichTextBlock()),
        ('image', ImageChooserBlock()),
    ])

    content_panels = AbstractEmailForm.content_panels + [
        StreamFieldPanel('intro'),
        ImageChooserPanel('image'),
        InlinePanel('form_fields', label="Form fields"),
        StreamFieldPanel('thank_you_text'),
        MultiFieldPanel([
            FieldRowPanel([
                FieldPanel('from_address', classname="col6"),
                FieldPanel('to_address', classname="col6"),
            ]),
            FieldPanel('subject'),
        ], "Email"),
    ]

    promote_panels = AbstractEmailForm.promote_panels + [
        FieldPanel('collection'),
    ]

    search_fields = AbstractEmailForm.search_fields + [
        index.SearchField('intro'),
    ]

    parent_page_types = ['home.HomePage']
    subpage_types: List[str] = []


class EventIndexPage(Page):
    intro = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname='full'),
    ]

    parent_page_types = ['home.HomePage']
    max_count = 1
    subpage_types = ['home.EventPage']

    search_fields = Page.search_fields + [
        index.SearchField('intro'),
    ]

    def get_next(self):
        now = timezone.now()
        return self.get_children().live().filter(eventpage__begin__gt=now)\
            .order_by('eventpage__begin').first()

    def get_context(self, request):
        context = super().get_context(request)
        children = self.get_children().order_by('-eventpage__begin')
        paginator = Paginator(children, settings.EVENT_ITEMS_PER_PAGE)
        page = request.GET.get('page')
        try:
            context['events'] = paginator.page(page)
        except PageNotAnInteger:
            context['events'] = paginator.page(1)
        except EmptyPage:
            context['events'] = paginator.page(paginator.num_pages)
        return context


class EventPage(Page):
    begin = models.DateTimeField()
    duration = models.DurationField(blank=True, null=True)
    end = models.DateTimeField(blank=True, null=True)
    all_day = models.BooleanField(default=False)
    description = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('begin'),
        FieldPanel('end'),
        FieldPanel('duration'),
        FieldPanel('all_day'),
        FieldPanel('description'),
    ]

    search_fields = Page.search_fields + [
        index.SearchField('description'),
    ]

    parent_page_types = ['home.EventIndexPage']
    subpage_types: List[str] = []


class AllergenesPage(Page):
    body = StreamField([
        ('titre', TitleSectionBlock()),
        ('section', SectionBlock()),
        ('texte', blocks.RichTextBlock(template='blocks/text.html')),
        ])
    collection = models.ForeignKey(Collection, on_delete=models.SET_NULL,
                                   related_name='+', null=True, blank=True)

    content_panels = Page.content_panels + [
        StreamFieldPanel('body'),
    ]

    promote_panels = Page.promote_panels + [
        FieldPanel('collection'),
    ]

    search_fields = Page.search_fields + [
        index.SearchField('body'),
    ]

    parent_page_types = ['home.HomePage']
    subpage_types: List[str] = []

    def get_context(self, request):
        context = super().get_context(request)
        alg = defaultdict(set)
        for meal in MealPage.objects.all():
            for ingredient in filter(lambda i: dict(i.value)['is_allergenic'],
                                     meal.ingredients):
                ingredient = dict(ingredient.value)
                allergene = ingredient['allergene'] or ingredient['ingredient']
                alg[allergene].add(meal)
        context['allergenes'] = dict(alg)
        return context
